﻿using daBackup.Exceptions;
using daBackup.Utils;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace daBackup
{
    public class DlExecutor
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly int NbAvailableThreads;
        private int NbCurrentThreads;
        private bool StorePerArtist;
        private String StoragePath;
        private IList<Deviation> DeviationList;
        private IList<Deviation> FailedDeviationList;
        private MainWindow ExecutorWindow;
        private IList<BackgroundWorker> WorkerList;

        /// <summary>
        /// Initializes a new DlExecutor object
        /// </summary>
        /// <param name="nbAvailableThreads">number of available threads to be used by the executor</param>
        /// <param name="storagePath">where is the "pictures" folder going to be created</param>
        /// <param name="deviationList">list of URLs pointing directly to the resources to download</param>
        /// <param name="storePerArtist">true to create a folder per artist and store downloaded art there; false to download everything in the "pictures" folder</param>
        public DlExecutor(int nbAvailableThreads, string storagePath, IList<Deviation> deviationList, bool storePerArtist)
        {
            this.NbAvailableThreads = nbAvailableThreads;
            this.NbCurrentThreads = 0;
            this.StorePerArtist = storePerArtist;
            this.StoragePath = storagePath;
            this.DeviationList = deviationList;
            this.FailedDeviationList = new List<Deviation>();
            this.WorkerList = new List<BackgroundWorker>();
        }

        /// <summary>
        /// Starts the execution of the multithreaded download procedure
        /// </summary>
        public void Run(object sender)
        {
            ExecutorWindow = sender as MainWindow;
            IList<IList<Deviation>> lists = SplitWork();

            for (int i = 0; i < NbAvailableThreads; i++)
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;
                worker.DoWork += new DoWorkEventHandler(worker_DoWork);
                worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
                WorkerList.Add(worker);
                NbCurrentThreads++;
                
                worker.RunWorkerAsync(lists[i]);
            }
        }

        /// <summary>
        /// Cancels the work of all active download threads
        /// </summary>
        public void Stop()
        {
            foreach (BackgroundWorker bw in WorkerList)
            {
                if (bw.WorkerSupportsCancellation == true)
                {
                    bw.CancelAsync();
                    bw.Dispose();
                }
            }

            WorkerList.Clear();
        }

        /// <summary>
        /// Splits the list of URLs to download into a given number of fragments.
        /// </summary>
        /// <param name="nbFragments">number of lists the original list must be split into</param>
        /// <returns></returns>
        protected IList<IList<Deviation>> SplitWork()
        {
            int nbTotItems = this.DeviationList.Count;
            int nbItemsPerFragment = (nbTotItems / NbAvailableThreads);
            IList<IList<Deviation>> result = new List<IList<Deviation>>();

            for (int i = 0; i < NbAvailableThreads; i++)
            {
                if ((i + 1) == NbAvailableThreads)
                    result.Add(new List<Deviation>(this.DeviationList.Skip(i * nbItemsPerFragment)));
                else
                    result.Add(new List<Deviation>(this.DeviationList.Skip(i * nbItemsPerFragment).Take(nbItemsPerFragment)));
                _logger.Debug("Added {0} items to the fragment {1}", result[i].Count, i);
            }

            return result;
        }

        /// <summary>
        /// Event triggered when the BackgroundWorker starts its job.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker currentWorker = sender as BackgroundWorker;
            IList<Deviation> deviationList = (IList<Deviation>)e.Argument;
            Random sleepInterval = new Random();
            string sanitizedLink;
            foreach (Deviation deviation in deviationList)
            {
                sanitizedLink = deviation.StorageUrl.Split(' ')[0];
                try
                {
                    try
                    {
                        DownloadUtils.Download(
                            sanitizedLink.RemoveWhitespaces(),
                            deviation.AuthorName,
                            deviation.Title,
                            StoragePath,
                            StorePerArtist
                        );
                    }
                    catch (ApiTemporaryFailureException tmpEx)
                    {
                        _logger.Debug(tmpEx, "Temporary failure detected: sleeping before retrying.");
                        Thread.Sleep(sleepInterval.Next(7000, 10000));
                        DownloadUtils.Download(
                            sanitizedLink.RemoveWhitespaces(),
                            deviation.AuthorName,
                            deviation.Title,
                            StoragePath,
                            StorePerArtist
                        );
                    }

                    currentWorker.ReportProgress(1);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Woops, something went wrong with the following url: '{0}'. See inner exception for more information.", sanitizedLink);
                    FailedDeviationList.Add(deviation);
                }
            }
        }

        /// <summary>
        /// Event triggered when the BackgroundWorker reports a progress.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ExecutorWindow.UpdateProgress();
        }

        /// <summary>
        /// Event triggered when the BackgroundWorker has done its job (success, error or cancel conditions).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker currentWorker = sender as BackgroundWorker;
            int threadNo = WorkerList.IndexOf(currentWorker);

            if (e.Cancelled)
            {
                _logger.Info("Thread {0} cancelled!", threadNo);
            }
            else if (!(e.Error == null))
            {
                _logger.Error("Thread {0} - ERROR: {1}", threadNo, e.Error.Message);
            }
            else
            {
                _logger.Info("Thread {0} completed its work!", threadNo);
                NbCurrentThreads--;

                if (NbCurrentThreads == 0)
                {
                    WorkerList.Clear();
                    ExecutorWindow.ReportWorkerComplete(DeviationList.Count, FailedDeviationList.Count, FailedDeviationList);
                }
            }

            currentWorker.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace daBackup.Exceptions
{
    public class ApiCallInvalidException : Exception
    {
        public ApiCallInvalidException() : base() { }

        public ApiCallInvalidException(string message) : base(message) { }

        public ApiCallInvalidException(string message, Exception innerException) : base(message, innerException) { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace daBackup.Exceptions
{
    public class ApiTemporaryFailureException : Exception
    {
        public ApiTemporaryFailureException() : base() { }

        public ApiTemporaryFailureException(string message) : base(message) { }

        public ApiTemporaryFailureException(string message, Exception innerException) : base(message, innerException) { }
    }
}

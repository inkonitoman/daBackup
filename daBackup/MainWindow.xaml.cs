﻿using daBackup.Exceptions;
using daBackup.Utils;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace daBackup
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private static readonly string DEFAULT_SETTINGS_PATH = String.Concat(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Path.DirectorySeparatorChar, "daBackup-settings.bin");
        private static readonly string DEFAULT_USER_HOME = (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX)
                                                                ? Environment.GetEnvironmentVariable("HOME")
                                                                : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");

        private static AppContext _context = AppContext.GetInstance();

        private DlExecutor executor;
        private bool isExecuting = false;

        public MainWindow()
        {
            InitializeComponent();
            InitializeApp();
        }

        public void InitializeApp()
        {
            // Since we are communicating with TLS-enabled services, better specify the accepted protocols here!
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            // Load saved AppContext (if any)
            if (File.Exists(DEFAULT_SETTINGS_PATH))
            {
                IFormatter formatter = new BinaryFormatter();
                using (Stream stream = new FileStream(DEFAULT_SETTINGS_PATH, FileMode.Open, FileAccess.Read, FileShare.Read))
                    AppContext.Restore((AppContext)formatter.Deserialize(stream));
            }

            UpdateButtonStatus();
            CheckForUpdates(false);
            
            if (String.IsNullOrEmpty(_context.DestinationPath))
                _context.DestinationPath = String.Concat(DEFAULT_USER_HOME, Path.DirectorySeparatorChar, "pictures", Path.DirectorySeparatorChar);
        }

        protected void SaveSettings()
        {
            // Save changes made to AppContext
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new FileStream(DEFAULT_SETTINGS_PATH, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                formatter.Serialize(stream, _context);
            }
        }

        protected void UpdateButtonStatus()
        {
            btnGo.IsEnabled = tbxUrl.Text != String.Empty && tbxUrl.Text.Contains("deviantart.com") && !isExecuting;
        }

        protected void CheckForUpdates(bool displayIfEquals)
        {
            Version latestVersion;
            Version localVersion = Assembly.GetEntryAssembly().GetName().Version;

            try
            {
                latestVersion = new Version(DownloadUtils.GetLastVersionNo());
            }
            catch (Exception ex)
            {
                latestVersion = localVersion;
                _logger.Error(ex, "Could not execute the version check.");
            }

            if (localVersion.CompareTo(latestVersion) < 0)
            {
                MessageBoxResult result = MessageBox.Show(
                    String.Format(
                        "daBackup v{0} is now available for download. (you currently run version {1}). Do you want to download it?",
                        latestVersion.ToString(),
                        localVersion.ToString()
                    ),
                    "Update available!",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Information
                );

                if (result == MessageBoxResult.Yes)
                {
                    string filePath = DownloadUtils.DownloadAppUpdate(_context.DestinationPath, latestVersion.ToString());
                    if (!String.IsNullOrEmpty(filePath))
                    {
                        System.Diagnostics.Process.Start("explorer.exe", "/select, \"" + filePath + "\"");
                        MessageBox.Show(
                            String.Format("We successfully downloaded the latest update for you. Please close daBackup and run the following file to proceed with the update: {0}", filePath),
                            "Ready to update",
                            MessageBoxButton.OK,
                            MessageBoxImage.Information
                        );
                    }
                    else
                        MessageBox.Show(
                            "Woops, there was an issue while downloading the update package. Please check your Internet connection and/or try later.",
                            "Error retrieving the update",
                            MessageBoxButton.OK,
                            MessageBoxImage.Error
                        );
                }
            }
            else if (localVersion.CompareTo(latestVersion) > 0)
                MessageBox.Show(
                    String.Format(
                        "You apparently are running a beta version of daBackup (v{0}). Please report all found issues.",
                        localVersion.ToString()
                    ),
                    "Beta version detected!",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning
                );
            else if (displayIfEquals && localVersion.CompareTo(latestVersion) == 0)
                MessageBox.Show(
                    "There aren't any updates available yet. You run the latest version, thank you!",
                    "No update available",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information
               );
        }

        public void UpdateProgress()
        {
            this.progressBar.Value += 1;
        }

        public void UpdateDetails(string message)
        {
            tbxDetails.Text += message + "\r\n";
        }

        private void ProceedWithDownload()
        {
            IList<Deviation> links = DownloadUtils.GetDownloadLinks(tbxUrl.Text, (int)slMaxItemsToDl.Value, _context.IsDownloadMatureContent);

            progressBar.Value = 0;
            progressBar.Maximum = links.Count;
            isExecuting = true;
            UpdateButtonStatus();

            this.executor = new DlExecutor(
                _context.NbMaxThreads,
                _context.DestinationPath,
                links,
                _context.IsSaveByArtist
            );
            this.Dispatcher.Invoke(DispatcherPriority.Background, new Action(delegate ()
            {
                executor.Run(this);
            }));
        }

        public void ReportWorkerComplete(int nbLinksProcessed, int nbFailedLinks, IList<Deviation> failedDeviationList)
        {
            if (nbFailedLinks > 0)
            {
                if (nbFailedLinks < nbLinksProcessed)
                    UpdateDetails(String.Format("{0} link(s) were fetched successfully, {1} link(s) were ignored (see CSV log file).", nbLinksProcessed - nbFailedLinks, nbFailedLinks));
                else
                    UpdateDetails("[ERROR] We couldn't download any link. Please check the log file for more information.");

                foreach (Deviation failedDeviation in failedDeviationList)
                {
                    _logger.Trace(failedDeviation.StorageUrl);
                }
            }
            else
            {
                UpdateDetails(String.Format("Successfully downloaded {0} links!", nbLinksProcessed - nbFailedLinks));
            }

            isExecuting = false;
            UpdateButtonStatus();

            MessageBox.Show("Download process finished", "Finished", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void btnGo_Click(object sender, RoutedEventArgs e)
        {
            try
            {                
                // Update GUI with download information
                UpdateDetails("Starting to process, this may take some time...");
                UpdateDetails(String.Format("Max deviations to download: {0}", (int)slMaxItemsToDl.Value));

                // Download items if recursive mode is not active or user confirms recursive mode's action
                ProceedWithDownload();
            }
            catch (ApiTemporaryFailureException atfex)
            {
                UpdateDetails(String.Format("[ERROR] {0}. Please make sure you are connected to the Internet and use a valid URL.", atfex.Message));
            }
            catch (Exception ex)
            {
                UpdateDetails(String.Format("[ERROR] {0}", ex.Message));
            }
        }

        private void tbxUrl_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateButtonStatus();
        }

        private void mOptions_Click(object sender, RoutedEventArgs e)
        {
            OptionsWindow window = new OptionsWindow();
            window.Owner = this;
            window.ShowDialog();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            SaveSettings();
        }

        private void mCheckUpdates_Click(object sender, RoutedEventArgs e)
        {
            CheckForUpdates(true);
        }

        private void mAbout_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow window = new AboutWindow();
            window.Owner = this;
            window.ShowDialog();
        }
    }
}

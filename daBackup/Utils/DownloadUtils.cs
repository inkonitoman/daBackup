﻿using daBackup.Exceptions;
using HtmlAgilityPack;
using ICSharpCode.SharpZipLib.GZip;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace daBackup.Utils
{
    public static class DownloadUtils
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private const int DEFAULT_OFFSET = 24; // WARNING: this property must stay identical to the minimum nb of links to download
        private const string URL_VERSION_CHECK = @"https://gitlab.com/inkonitoman/daBackup/-/raw/master/VERSION.txt";
        private const string URL_LATEST_VERSION_DL = @"https://gitlab.com/inkonitoman/daBackup/-/raw/master/Installers/daBackup_[ASSEMBLYV]_install.exe";
        private const string UPDATE_PKG_NAME = "daBackup_[ASSEMBLYV]_install.exe";
        private const string API_CLIENT_ID = "10812";
        private const string API_CLIENT_SECRET = "e10da9b61a1f9a54e7b247b5aec406e4";

        public static string GetAccessToken()
        {
            using (HttpClient client = new HttpClient())
            {
                Uri tokenUri = new Uri(
                    "https://www.deviantart.com/oauth2/token?client_id="
                    + API_CLIENT_ID
                    + "&client_secret="
                    + API_CLIENT_SECRET
                    + "&grant_type=client_credentials"
                );

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("User-Agent", "daBackup");
                client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip");

                // List data response.
                using (HttpResponseMessage response = client.GetAsync(tokenUri).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        // Parse the response body.
                        using (Stream data = new GZipInputStream(response.Content.ReadAsStreamAsync().Result))
                        {
                            using (StreamReader reader = new StreamReader(data))
                            {
                                var jsonMessage = reader.ReadToEnd();
                                JObject daResponse = JObject.Parse(jsonMessage);
                                try
                                {
                                    return (string)daResponse["access_token"];
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(
                                        "No access token could be obtained from DeviantArt: {0} ({1})",
                                        (string)daResponse["error"],
                                        (string)daResponse["error_description"]
                                    );
                                    throw new ApiTemporaryFailureException(
                                        String.Format(
                                            "No access token could be obtained from DeviantArt: {0} ({1})",
                                            (string)daResponse["error"],
                                            (string)daResponse["error_description"]
                                        )
                                    );
                                }
                            }
                        }
                    }
                    else
                    {
                        _logger.Error("GetAccessToken failed: HTTP {0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                        throw new ApiTemporaryFailureException(String.Format("The API service didn't respond as expected: HTTP {0} ({1})", (int)response.StatusCode, response.ReasonPhrase));
                    }
                }
            }
        }

        /// <summary>
        /// Based on a provided URL, fetch the page DOM and extract links to images.
        /// </summary>
        /// <param name="url">the URL of the page to fetch (i.e. https://inkonitoman.deviantart.com/gallery/) </param>
        /// <param name="nbMaxLinks">the maximum number of links to download</param>
        /// <param name="dlMature">true to list mature content; false otherwise</param>
        /// <returns>a list of exploitable links to pass to the DeviantArt API</returns>
        public static IList<Deviation> GetDownloadLinks(string url, int nbMaxLinks, bool dlMature)
        {
            IList<Deviation> links = new List<Deviation>();
            string token = GetAccessToken();
            return GetDownloadLinksRecursive(url, token, links, nbMaxLinks, 0, dlMature);
        }

        /// <summary>
        /// Download the image, animation or video at the given URL.
        /// </summary>
        /// <param name="url">the URL to the resource to download</param>
        /// <param name="authorName">the name of the deviation author</param>
        /// <param name="title">the title of the deviation</param>
        /// <param name="destPath">the path where to download the resource</param>
        /// <param name="storePerArtist">true to create a folder per artist and store downloaded art there; false to download everything in the "pictures" folder</param>
        public static void Download(string url, string authorName, string title, string destPath, bool storePerArtist)
        {
            if (storePerArtist)
                DownloadImage(url, destPath + authorName + Path.DirectorySeparatorChar, title);
            else
                DownloadImage(url, destPath, title);
        }

        /// <summary>
        /// Checks the latest version number published on the online code repository.
        /// </summary>
        /// <returns>the currently published version number</returns>
        public static string GetLastVersionNo()
        {
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("User-Agent: daBackup");

                using (Stream data = client.OpenRead(new Uri(URL_VERSION_CHECK)))
                {
                    using (StreamReader reader = new StreamReader(data))
                    {
                        return reader.ReadLine();
                    }
                }
            }
        }

        /// <summary>
        /// Downloads the latest version of the application to the specified path
        /// </summary>
        /// <param name="path">the path where to store the update file</param>
        /// <param name="version">the version to download from the repository</param>
        /// <returns></returns>
        public static string DownloadAppUpdate(string path, string version)
        {
            string updatePackageLocation = path + UPDATE_PKG_NAME.Replace("[ASSEMBLYV]", version.Replace('.', '-'));
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("User-Agent: daBackup");

                try
                {
                    client.DownloadFile(URL_LATEST_VERSION_DL.Replace("[ASSEMBLYV]", version.Replace('.', '-')), updatePackageLocation);
                    return updatePackageLocation;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "An error happened during the execution of DownloadLatestUpdate.");
                    return "";
                }
            }
        }

        // -----------------------------------------------------------
        // Private methods (do not expose these!)
        // -----------------------------------------------------------

        /// <summary>
        /// Finds the username from a DeviantArt URL.
        /// 
        /// (i.e. https://www.deviantart.com/abc will return "abc")
        /// (i.e. https://abc.deviantart.com/gallery will return "abc")
        /// </summary>
        /// <param name="url">the url of the deviations to download</param>
        /// <returns>the username of the dA account</returns>
        private static string GetUsernameFromUrl(string url)
        {
            string[] urlParts = url.Split('.');
            string firstPart = urlParts[0].Split('/')[2];
            if (firstPart == "www") {
                urlParts = url.Split('/');
                return urlParts[3];
            }
            return firstPart;
        }

        /// <summary>
        /// Indicates whether the provided link is a gallery or a collection.
        /// </summary>
        /// <param name="url">the URL to get </param>
        /// <returns></returns>
        private static bool IsLinkAGallery(string url)
        {
            if (url.Contains("gallery"))
                return true;
            return false;
        }

        /// <summary>
        /// Gets the "Featured" gallery folder UUID from DeviantArt.
        /// </summary>
        /// <param name="username">the username of the user to fetch the gallery folders from</param>
        /// <returns>the UUID of the "Featured" gallery folder</returns>
        private static string GetGalleryUUID(string username)
        {
            using (HttpClient client = new HttpClient())
            {
                // Get an access token for the API calls
                string token = GetAccessToken();
                Uri endpointUri = new Uri(
                    String.Format(
                        "https://www.deviantart.com/api/v1/oauth2/gallery/folders?username={0}&access_token={1}",
                        username,
                        token
                    )
                );

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("User-Agent", "daBackup");
                client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip");

                // List data response.
                using (HttpResponseMessage response = client.GetAsync(endpointUri).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        // Parse the response body.
                        using (Stream data = new GZipInputStream(response.Content.ReadAsStreamAsync().Result))
                        {
                            using (StreamReader reader = new StreamReader(data))
                            {
                                var jsonMessage = reader.ReadToEnd();
                                JObject daResponse = JObject.Parse(jsonMessage);
                                try
                                {
                                    // Find all folders of the user's gallery and parse them
                                    JArray results = (JArray)daResponse["results"];
                                    foreach (JToken result in results)
                                    {
                                        // If a "Featured" folder exists, use it!
                                        if ("Featured" == (string)result["name"])
                                            return (string)result["folderid"];
                                    }

                                    // If no "Featured" folder was found, throw an exception
                                    _logger.Error(
                                        "Could not retrieve the 'Featured' gallery folder of user '{0}'. Found: '{1}'.",
                                        username,
                                        String.Join(", ", results.Select(r => (string)r["name"]).ToArray())
                                    );
                                    throw new ApiTemporaryFailureException(
                                        String.Format(
                                            "Could not retrieve the 'Featured' gallery folder of user '{0}'.",
                                            username
                                        )
                                    );
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(
                                        "Could not retrieve gallery folders of user '{0}': {1} ({2})",
                                        username,
                                        (string)daResponse["error"],
                                        (string)daResponse["error_description"]
                                    );
                                    throw new ApiTemporaryFailureException(
                                        String.Format(
                                            "Could not retrieve gallery folders of user '{0}': {1} ({2})",
                                            username,
                                            (string)daResponse["error"],
                                            (string)daResponse["error_description"]
                                        )
                                    );
                                }
                            }
                        }
                    }
                    else
                    {
                        _logger.Error("GetGalleryUUID failed: HTTP {0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                        throw new ApiTemporaryFailureException(String.Format("The API service didn't respond as expected: HTTP {0} ({1})", (int)response.StatusCode, response.ReasonPhrase));
                    }
                }
            }
        }

        /// <summary>
        /// Gets the "Featured" collection folder UUID from DeviantArt.
        /// </summary>
        /// <param name="username">the username of the user to fetch the collection folders from</param>
        /// <returns>the UUID of the "Featured" collection folder</returns>
        private static string GetCollectionUUID(string username)
        {
            using (HttpClient client = new HttpClient())
            {
                string token = GetAccessToken();
                Uri endpointUri = new Uri(
                    String.Format(
                        "https://www.deviantart.com/api/v1/oauth2/collections/folders?username={0}&access_token={1}",
                        username,
                        token
                    )
                );

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("User-Agent", "daBackup");
                client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip");

                // List data response.
                using (HttpResponseMessage response = client.GetAsync(endpointUri).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        // Parse the response body.
                        using (Stream data = new GZipInputStream(response.Content.ReadAsStreamAsync().Result))
                        {
                            using (StreamReader reader = new StreamReader(data))
                            {
                                var jsonMessage = reader.ReadToEnd();
                                JObject daResponse = JObject.Parse(jsonMessage);
                                try
                                {
                                    // Find all collections of the user and parse them
                                    JArray results = (JArray)daResponse["results"];
                                    foreach (JToken result in results)
                                    {
                                        // If a "Featured" collection exists, use it!
                                        if ("Featured" == (string)result["name"])
                                            return (string)result["folderid"];
                                    }

                                    // If no "Featured" collection was found, throw an exception
                                    _logger.Error(
                                        "Could not retrieve the 'Featured' collections of user '{0}'. Found: '{1}'.",
                                        username,
                                        String.Join(", ", results.Select(r => (string)r["name"]).ToArray())
                                    );
                                    throw new ApiTemporaryFailureException(
                                        String.Format(
                                            "Could not retrieve the 'Featured' collections of user '{0}'.",
                                            username
                                        )
                                    );
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(
                                        "Could not retrieve user collections of user '{0}': {1} ({2})",
                                        username,
                                        (string)daResponse["error"],
                                        (string)daResponse["error_description"]
                                    );
                                    throw new ApiTemporaryFailureException(
                                        String.Format(
                                            "Could not retrieve user collections of user '{0}': {1} ({2})",
                                            username,
                                            (string)daResponse["error"],
                                            (string)daResponse["error_description"]
                                        )
                                    );
                                }
                            }
                        }
                    }
                    else
                    {
                        _logger.Error("GetCollectionUUID failed: HTTP {0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                        throw new ApiTemporaryFailureException(String.Format("The API service didn't respond as expected: HTTP {0} ({1})", (int)response.StatusCode, response.ReasonPhrase));
                    }
                }
            }
        }

        /// <summary>
        /// Query DeviantArt's API to get the details of an image/animation/video.
        /// </summary>
        /// <param name="url">the URL of the art you're looking for</param>
        /// <returns>a JSON message from the API</returns>
        [Obsolete("daBackup v3.0 relies on the OAuth2 API wheras this endpoint relies on oEmbed")]
        private static string GetJSON(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://backend.deviantart.com/oembed");

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("User-Agent", "daBackup");
                client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip");

                // List data response.
                using (HttpResponseMessage response = client.GetAsync("?url=" + url).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        // Parse the response body.
                        using (Stream data = new GZipInputStream(response.Content.ReadAsStreamAsync().Result))
                        {
                            using (StreamReader reader = new StreamReader(data))
                            {
                                var jsonMessage = reader.ReadToEnd();
                                _logger.Info("JSON message received from '{0}'", url);
                                return jsonMessage;
                            }
                        }
                    }
                    else
                    {
                        _logger.Error("GetJSON failed for url '{0}': HTTP {1} ({2})", url, (int)response.StatusCode, response.ReasonPhrase);
                        throw new ApiTemporaryFailureException(String.Format("The API service didn't respond as expected: HTTP {0} ({1})", (int)response.StatusCode, response.ReasonPhrase));
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves deviations from a user's gallery or collections according to a specific offset
        /// and a limit defined by DEFAULT_OFFSET.
        /// </summary>
        /// <param name="url">the url of the deviations to download</param>
        /// <param name="token">the OAuth2 token sent by dA</param>
        /// <param name="offset">the offset to consider</param>
        /// <param name="dlMature">true to list mature content links; false otherwise</param>
        /// <returns>a list of links where deviations can be found</returns>
        private static IList<Deviation> GetDeviationsFromAPI(string url, string token, int offset, bool dlMature)
        {
            string endpointType = IsLinkAGallery(url) ? "gallery" : "collections";
            string username = GetUsernameFromUrl(url);
            IList<Deviation> resultList = new List<Deviation>();

            string folderUUID;
            if (endpointType == "gallery")
                folderUUID = GetGalleryUUID(username);
            else
                folderUUID = GetCollectionUUID(username);

            // Fetch deviations from the API
            using (HttpClient client = new HttpClient())
            {
                Uri endpointUri = new Uri(
                    String.Format(
                        "https://www.deviantart.com/api/v1/oauth2/{0}/{1}?username={2}&offset={3}&limit={4}&mature_content={5}&mode=newest&access_token={6}",
                        endpointType,
                        folderUUID,
                        username,
                        offset,
                        DEFAULT_OFFSET,
                        dlMature.ToString().ToLower(),
                        token
                    )
                );

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("User-Agent", "daBackup");
                client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip");

                // List data response.
                using (HttpResponseMessage response = client.GetAsync(endpointUri).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        // Parse the response body.
                        using (Stream data = new GZipInputStream(response.Content.ReadAsStreamAsync().Result))
                        {
                            using (StreamReader reader = new StreamReader(data))
                            {
                                var jsonMessage = reader.ReadToEnd();
                                string storageUrl;
                                JObject daResponse = JObject.Parse(jsonMessage);
                                try
                                {
                                    // Find all collections of the user and parse them
                                    JArray results = (JArray)daResponse["results"];
                                    foreach (JToken result in results)
                                    {
                                        storageUrl = GetStorageUrl(result);
                                        if (!storageUrl.Equals(string.Empty))
                                        {
                                            Deviation deviation = new Deviation(
                                                GetAuthorName(result),
                                                GetTitle(result),
                                                GetFileExt(storageUrl),
                                                storageUrl
                                            );
                                            resultList.Add(deviation);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(
                                        "Could not retrieve deviations of user '{0}': {1} ({2})",
                                        username,
                                        (string)daResponse["error"],
                                        (string)daResponse["error_description"]
                                    );
                                    throw new ApiTemporaryFailureException(
                                        String.Format(
                                            "Could not retrieve deviations of user '{0}': {1} ({2})",
                                            username,
                                            (string)daResponse["error"],
                                            (string)daResponse["error_description"]
                                        )
                                    );
                                }
                            }
                        }
                        return resultList;
                    }
                    else
                    {
                        _logger.Error("GetDeviationsFromAPI failed: HTTP {0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                        throw new ApiTemporaryFailureException(String.Format("The API service didn't respond as expected: HTTP {0} ({1})", (int)response.StatusCode, response.ReasonPhrase));
                    }
                }
            }
        }

        /// <summary>
        /// Finds out where the art is being stored based on a previous call to the API.
        /// </summary>
        /// <param name="jsonResult">the in-memory JSON structure containing a deviation</param>
        /// <returns>the URL to the art</returns>
        private static string GetStorageUrl(JToken jsonResult)
        {
            try
            {
                return (string)jsonResult["content"]["src"];
            }
            catch (Exception ex)
            {
                _logger.Warn(ex, "Could not find picture: JSON structure is invalid.");
                return String.Empty;
                //throw new ApiCallInvalidException("Could not find picture: JSON structure is invalid.", ex);
            }
        }

        /// <summary>
        /// Obtains the author's name from a previous call to the API
        /// </summary>
        /// <param name="jsonResult">the in-memory JSON structure containing a deviation</param>
        /// <returns>the name of the author of the art</returns>
        private static string GetAuthorName(JToken jsonResult)
        {
            try
            {
                return (string)jsonResult["author"]["username"];
            }
            catch (Exception ex)
            {
                _logger.Debug(ex, "Could not find author's name: JSON structure is invalid.");
                throw new ApiCallInvalidException("Could not find author's name: JSON structure is invalid.", ex);
            }
        }

        /// <summary>
        /// Obtains the title of the resource from the API
        /// </summary>
        /// <param name="jsonResult">the in-memory JSON structure containing a deviation</param>
        /// <returns>the title of the art</returns>
        private static string GetTitle(JToken jsonResult)
        {
            try
            {
                return (string)jsonResult["title"];
            }
            catch (Exception ex)
            {
                _logger.Debug(ex, "Could not find author's name: JSON structure is invalid.");
                throw new ApiCallInvalidException("Could not find author's name: JSON structure is invalid.", ex);
            }
        }

        /// <summary>
        /// Obtains the file extension of a file to download.
        /// </summary>
        /// <param name="url">the url of the file to download</param>
        /// <returns>the file extension (exemple: jpg)</returns>
        private static string GetFileExt(string url)
        {
            string[] mimePart = url.Split('?');
            mimePart = mimePart[0].Split('.');
            return mimePart[mimePart.Length - 1];
        }

        /// <summary>
        /// Downloads links in a recursive (or single) way.
        /// </summary>
        /// <param name="url">the start URL from where to download links</param>
        /// <param name="token">the OAuth2 authorization token to access dA's API</param>
        /// <param name="deviations">a list of links where this method will store new links</param>
        /// <param name="nbMaxLinks">the maximum number of links to download</param>
        /// <param name="offset">the current offset to apply</param>
        /// <param name="dlMature">true to list mature content links; false otherwise</param>
        /// <returns>a list of links to download resources from</returns>
        private static IList<Deviation> GetDownloadLinksRecursive(string url, string token, IList<Deviation> deviations, int nbMaxLinks, int offset, bool dlMature)
        {
            if (nbMaxLinks >= deviations.Count + offset)
            {
                IList<Deviation> apiResults = GetDeviationsFromAPI(url, token, offset, dlMature);
                foreach (Deviation d in apiResults)
                    deviations.Add(d);

                return GetDownloadLinksRecursive(url, token, deviations, nbMaxLinks, deviations.Count + offset, dlMature);
            }

            return deviations;
        }

        /// <summary>
        /// Downloads the image/animation/video from its original location.
        /// </summary>
        /// <param name="url">the URL where the art is stored (see <see cref="GetStorageUrl"/>)</param>
        /// <param name="destPath">the path the art must be downloaded to</param>
        /// <param name="title">name of the art</param>
        private static void DownloadImage(string url, string destPath, string title)
        {
            if (!Directory.Exists(destPath))
                Directory.CreateDirectory(destPath);

            using (WebClient client = new WebClient())
            {
                try
                {
                    string fileExt = "." + GetFileExt(url);
                    _logger.Debug("Attempting to download: " + url + " " + destPath + title + fileExt);
                    client.DownloadFile(new Uri(url), destPath + title + fileExt);
                }
                catch (Exception ex)
                {
                    _logger.Debug(ex, "Could not download file: " + ex.Message);
                    throw new ApiCallInvalidException("Could not download file: " + ex.Message, ex);
                }
            }
        }

        /// <summary>
        /// Find the offset parameter in a DeviantArt URL.
        /// </summary>
        /// <param name="url">the URL to search</param>
        /// <returns>the actual offset if it exists; 0 if it doesn't</returns>
        [Obsolete("Deprecated since recursive mode is not usable anymore with the OAuth2 API.")]
        private static int GetOffset(string url)
        {
            string[] urlParts = url.Split('?', '&');
            foreach (string part in urlParts)
            {
                if (part.Contains("offset="))
                {
                    int offset = 0;
                    string offsetStr = part.Split('=')[1];
                    if (Int32.TryParse(offsetStr, out offset) && offset >= 0)
                        return offset;
                }
            }

            // By default, the latest page has an offset of 0, so whenever we would not be able to find it, we'll just use that.
            return 0;
        }

        /// <summary>
        /// Generates the next URL to download links from, based on DeviantArt's offset parameter.
        /// </summary>
        /// <param name="url">the base URL, such as http://username.deviantart.com/favorites/?offset=576 </param>
        /// <returns>the next URL with a shorter offset, for instance http://username.deviantart.com/favorites/?offset=552 </returns>
        [Obsolete("Deprecated since recursive mode is not usable anymore with the OAuth2 API.")]
        private static string GetNextUrl(string url)
        {
            int offset = GetOffset(url);
            int newOffset = (offset - DEFAULT_OFFSET) >= 0 ? (offset - DEFAULT_OFFSET) : 0; // enforce new offset cannot be negative

            return offset == newOffset ? String.Empty : url.Replace("offset=" + offset, "offset=" + newOffset);
        }
    }
}

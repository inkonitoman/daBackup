﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace daBackup
{
    public class Deviation
    {
        public string AuthorName { get; set; }
        public string Title { get; set; }
        public string FileExtension { get; set; }
        public string StorageUrl { get; set; }

        public Deviation() { }

        public Deviation(string author, string title, string fileExt, string url)
        {
            this.AuthorName = author;
            this.Title = title;
            this.FileExtension = fileExt;
            this.StorageUrl = url;
        }
    }
}

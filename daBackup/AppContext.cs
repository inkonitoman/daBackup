﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace daBackup
{
    [Serializable]
    public class AppContext
    {
        [NonSerialized]
        protected static AppContext _context;
        
        public int NbMaxThreads { get; set; }
        public bool IsSaveByArtist { get; set; }
        public bool IsDownloadMatureContent { get; set; }
        public string DestinationPath { get; set; }

        private AppContext() {
            this.NbMaxThreads = 2;
            this.IsSaveByArtist = true;
            this.IsDownloadMatureContent = true;
        }

        public static AppContext GetInstance()
        {
            if (_context == null)
                _context = new AppContext();

            return _context;
        }

        public static void Restore(AppContext savedContext)
        {
            AppContext context = GetInstance();
            context.NbMaxThreads = savedContext.NbMaxThreads;
            context.IsSaveByArtist = savedContext.IsSaveByArtist;
            context.IsDownloadMatureContent = savedContext.IsDownloadMatureContent;
            context.DestinationPath = savedContext.DestinationPath;
        }
    }
}

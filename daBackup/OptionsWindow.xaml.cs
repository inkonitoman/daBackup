﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace daBackup
{
    /// <summary>
    /// Logique d'interaction pour OptionsWindow.xaml
    /// </summary>
    public partial class OptionsWindow : Window
    {
        protected static AppContext _context = AppContext.GetInstance();

        public OptionsWindow()
        {
            InitializeComponent();
            InitializeWindow();
        }

        private void InitializeWindow()
        {
            cbxSaveByArtist.IsChecked = _context.IsSaveByArtist;
            cbxDownloadMature.IsChecked = _context.IsDownloadMatureContent;
            tbxDestFolder.Text = _context.DestinationPath;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnBrowseDestFolder_Click(object sender, RoutedEventArgs e)
        {
            // Technical hack to get a folder browser in WPF from: https://stackoverflow.com/questions/4007882/select-folder-dialog-wpf/#50261897
            var dialog = new Microsoft.Win32.SaveFileDialog();
            dialog.InitialDirectory = tbxDestFolder.Text; // Use current value for initial dir
            dialog.Title = "Select a directory"; // instead of default "Save As"
            dialog.Filter = "Directory|*.directory"; // Prevents displaying files
            dialog.FileName = "select";
            if (dialog.ShowDialog() == true)
            {
                string path = dialog.FileName;
                // Remove fake filename from resulting path
                path = path.Replace("\\select.directory", "");
                path = path.Replace(".directory", "");
                // If user has changed the filename, create the new directory
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                // Our final value is in path
                tbxDestFolder.Text = path + System.IO.Path.DirectorySeparatorChar;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            _context.IsSaveByArtist = cbxSaveByArtist.IsChecked.HasValue ? cbxSaveByArtist.IsChecked.Value : true;
            _context.IsDownloadMatureContent = cbxDownloadMature.IsChecked.HasValue ? cbxDownloadMature.IsChecked.Value : true;
            _context.DestinationPath = String.IsNullOrEmpty(tbxDestFolder.Text) ? _context.DestinationPath : tbxDestFolder.Text;

            this.Close();
        }
    }
}

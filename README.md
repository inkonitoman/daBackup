# daBackup - by InkonitoMan

## Introduction
daBackup is a small application which aim to help you downloading your gallery items or favorites from DeviantArt. Even if this site has got a nice community and cool features, I somehow don't trust them enough to let them keep my favorite pieces of art on their servers by their own. What if they go bankrupt or decide to shutdown everything? It may not happen, I don't know, but if you want to keep a trace of what you love then this tool is for you.

## Important notice
Even if daBackup is functional, it still has to evolve a little bit (some interesting features may arise regularly) and some bugs may still be discovered.

Also note this tool is provided **as-is** with no specific assistance or guarantee. We'll do our best to support you though!

## Where to get more information?
You can find more information on [our official wiki pages](https://gitlab.com/inkonitoman/daBackup/wikis/home).